--4.2
SELECT model, range
FROM aircrafts;

--4.3:
--1) либо больше 10 000 км, либо меньше 4 000 км;
SELECT model, range
FROM aircrafts
WHERE range > 10000 
  OR 
  range < 4000;

--2) больше 6 000 км, а название не заканчивается на «100».
SELECT model, range
FROM aircrafts
WHERE range > 6000 
  AND 
  NOT model LIKE '%100';

--4.4
SELECT flight_no, scheduled_departure
FROM flights
WHERE status = 'Arrived' 
  AND actual_arrival > scheduled_arrival;

--4.5
SELECT COUNT(*)
FROM flights
WHERE departure_airport = 'LED'
  AND status = 'Cancelled'
  AND (to_char(scheduled_departure, 'Day')) ~ 'Thursday'
  AND (to_char(scheduled_arrival, 'Day')) ~ 'Thursday';

--4.6
SELECT passenger_name
FROM tickets t
  JOIN ticket_flights tf ON t.ticket_no = tf.ticket_no
WHERE fare_conditions = 'Economy'
  AND amount > 70000;

--4.7
SELECT t.passenger_name,
  f.departure_airport   AS d_airport,
  f.arrival_airport     AS a_airport,
  f.scheduled_departure AS departure_time,
  f.scheduled_arrival   AS arrival_time,
  bp.seat_no            AS seat
FROM flights f
  JOIN ticket_flights tf  ON f.flight_id = tf.flight_id
  JOIN tickets t          ON t.ticket_no = tf.ticket_no
  JOIN boarding_passes bp ON bp.ticket_no = tf.ticket_no
WHERE status = 'Scheduled';

--4.8
SELECT t.passenger_name,
  f.flight_no
FROM tickets t
  JOIN ticket_flights tf       ON t.ticket_no = tf.ticket_no
  JOIN flights f               ON f.flight_id = tf.flight_id
  LEFT JOIN boarding_passes bp ON t.ticket_no = bp.ticket_no
    AND bp.ticket_no IS NULL
WHERE f.scheduled_departure = bookings.now();

--4.9
SELECT s.seat_no,
  f.flight_no,
  f.actual_departure
FROM flights f
  JOIN aircrafts a              ON f.aircraft_code = a.aircraft_code
  JOIN seats s                  ON a.aircraft_code = s.aircraft_code
  LEFT JOIN boarding_passes bp  ON s.seat_no = bp.seat_no
    AND bp.seat_no IS NULL
WHERE f.departure_airport = 'AAQ'
  AND f.arrival_airport = 'SVO'
  AND f.actual_departure IS NOT NULL;

--4.10
SELECT ROUND(AVG(tf.amount), 2)
FROM ticket_flights tf, flights f
WHERE tf.flight_id = f.flight_id
  AND f.departure_airport = 'VOZ'
  AND f.arrival_airport = 'LED';

--4.11
SELECT ROUND(AVG(amount), 2) AS average_price,
  CASE 
    WHEN fare_conditions ~ 'Business' THEN 'Бизнес'
    WHEN fare_conditions ~ 'Economy'  THEN 'Эконом'
    ELSE 'Комфорт'
  END AS fare_class
FROM ticket_flights
GROUP BY fare_class;

--4.12
SELECT a.model     AS Aircraft_model, 
  COUNT(s.seat_no) AS Total_seats_amount
FROM aircrafts a, seats s
WHERE a.aircraft_code = s.aircraft_code
GROUP BY a.model;

--4.13
SELECT f.arrival_airport,
  COUNT(f.flight_no) AS total_flights
FROM flights f
WHERE f.actual_departure IS NOT NULL
GROUP BY f.arrival_airport
HAVING COUNT(f.flight_no) > 500;

--4.14
INSERT INTO seats (seat_no, aircraft_code, fare_conditions)
VALUES ('7A', 'CN1', 'Economy');

INSERT INTO seats (seat_no, aircraft_code, fare_conditions)
VALUES ('7B', 'CN1', 'Economy');

--4.15
UPDATE seats s
SET fare_conditions = 'Business'
FROM aircrafts a
WHERE s.aircraft_code = '319'
  AND s.seat_no BETWEEN '6A' AND '8F'
  AND s.aircraft_code = a.aircraft_code
RETURNING s.seat_no, s.aircraft_code, a.model, s.fare_conditions;

--4.16
INSERT INTO bookings (book_ref, book_date, total_amount)
VALUES ('_00721', bookings.now(), 30000);

INSERT INTO tickets (
  ticket_no, 
  book_ref, 
  passenger_id, 
  passenger_name, 
  contact_data
  )
VALUES (
  '7777777777777', 
  '_00721', 
  '2121 111777', 
  'ANDREI SATOR', 
  '{"phone": "+79146784823"}');

INSERT INTO flights (
  flight_id, 
  flight_no, 
  scheduled_departure, 
  scheduled_arrival, 
  departure_airport, 
  arrival_airport, 
  status,
  aircraft_code
  )
VALUES (
  999999, 
  'PG8888', 
  bookings.now() + INTERVAL '7 days',
  bookings.now() + INTERVAL '7 days 8 hours 23 minutes',
  'VKO',
  'VVO',
  'Scheduled',
  'CN1'
);

INSERT INTO flights (
  flight_id, 
  flight_no, 
  scheduled_departure, 
  scheduled_arrival, 
  departure_airport, 
  arrival_airport, 
  status,
  aircraft_code
  )
VALUES (
  9999999, 
  'PG9999',
  bookings.now() + INTERVAL '14 days',
  bookings.now() + INTERVAL '14 days 8 hours 30 minutes',
  'VVO',
  'VKO',
  'Scheduled',
  'CN1'
);

INSERT INTO ticket_flights (
  ticket_no,
  flight_id,
  fare_conditions,
  amount
)
VALUES (
  7777777777777,
  999999,
  'Economy',
  15000
);

INSERT INTO ticket_flights (
  ticket_no,
  flight_id,
  fare_conditions,
  amount
)
VALUES (
  7777777777777,
  9999999,
  'Economy',
  15000
);

--4.17
ALTER TABLE boarding_passes
ADD COLUMN fare_class_upgrade VARCHAR(8);

ALTER TABLE boarding_passes
ADD COLUMN class_upgrade_fee INTEGER;

--4.18
CREATE TABLE regular_passengers (
  passenger_id VARCHAR(20) NOT NULL,
  passenger_name TEXT NOT NULL,
  contact_data jsonb,
  discount_percent INTEGER,
  PRIMARY KEY (passenger_id)
);

--4.19
ALTER TABLE regular_passengers
ADD COLUMN pet_animals TEXT;

--4.20
SELECT model
FROM aircrafts
WHERE aircraft_code IN (
  SELECT aircraft_code
  FROM flights
  WHERE (actual_arrival - actual_departure) > INTERVAL '6 hours'
);

--4.21
SELECT COUNT(flight_id)
FROM flights
WHERE (actual_departure - scheduled_departure) > INTERVAL '4 hours';

--4.22
WITH ppy AS (
  SELECT COUNT(tf.ticket_no) AS passengers_per_year,
  a.airport_code
  FROM ticket_flights tf
    JOIN flights f  ON f.flight_id = tf.flight_id
    JOIN airports a ON f.departure_airport = a.airport_code 
      OR f.arrival_airport = a.airport_code
  WHERE f.scheduled_departure BETWEEN bookings.now() - INTERVAL '1 year' AND bookings.now()
    OR  f.scheduled_arrival   BETWEEN bookings.now() - INTERVAL '1 year' AND bookings.now()
  GROUP BY 2
  ORDER BY passengers_per_year DESC
)
SELECT a.airport_name,
  ROUND(ppy.passengers_per_year / 365, 0) AS average_passengers_per_day
FROM ppy, airports a
WHERE ppy.airport_code = a.airport_code
GROUP BY 1,2
ORDER BY average_passengers_per_day DESC
LIMIT 10;

--4.23
SELECT COUNT(f.flight_id)
FROM flights f, flights fn
WHERE f.departure_airport = 'SVO'
  AND fn.departure_airport = 'SVO'
  AND f.actual_departure <> fn.actual_departure
  AND fn.actual_departure > f.actual_departure
  AND fn.actual_departure - f.actual_departure < INTERVAL '5 minutes';